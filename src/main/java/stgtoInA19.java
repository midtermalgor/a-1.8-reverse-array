import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author satit
 */
public class stgtoInA19 {

    static int getValuesofint(char i) {
        if (i == '1') {
            return 1;
        } else if (i == '2') {
            return 2;
        } else if (i == '3') {
            return 3;
        } else if (i == '4') {
            return 4;
        } else if (i == '5') {
            return 5;
        } else if (i == '6') {
            return 6;
        } else if (i == '7') {
            return 7;
        } else if (i == '8') {
            return 8;
        } else if (i == '9') {
            return 9;
        }
        return 0;
    }

    static int covertStringtoDigit(String words) {
        long startTime = System.nanoTime();
        int result = Integer.parseInt(words);

        int resultInteger = 0;
        for (int i = 0; i < words.length(); i++) {
            char numString = words.charAt(i);
            if (numString == '0')
                continue;
            int num = getValuesofint(numString);
            int valueMul = (words.length() - 1) - i;
            for (int m = 0; m < valueMul; m++) {
                num *= 10;
            }
            resultInteger += num;
        }

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Time: " + totalTime + " nanoseconds");
        return resultInteger;
    }

    static String readInput(String filetxt){
        String wordInteger = " ";
        try {
            Scanner numFile = new Scanner(new File(filetxt));
            while (numFile.hasNextLine()) {
                wordInteger = numFile.nextLine();
            }
            numFile.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Error");

        }
        return wordInteger;
        
    }

    static void outputResult(String filetxt, int integer) {
        try {
            FileWriter writter = new FileWriter(filetxt);
            writter.write(Integer.toString(integer));
            writter.close();
            System.out.println("output: " + integer + "\nWrited: " + filetxt);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Please insert input and output file\nExample: java program input.txt output.txt");
            return;
        } else {
            if (!args[0].contains(".txt") || !args[1].contains(".txt")) {
                System.out.println("Support only .txt file input and output");
                return;
            }
        }
        String inputfiletxt = args[0];
        String outputfiletxt = args[1];

        String integerString = readInput(inputfiletxt);
        int integer = covertStringtoDigit(integerString);
        outputResult(outputfiletxt, integer);
    }
}
